use std::sync::{Arc, Mutex};

fn main() {

    //let mut numbers = vec![1i, 2i, 3i];
    let numbers = Arc::new(Mutex::new(vec![1i, 2i, 3i]));

    // 10個のスレッドを作る
    for i in range(0u, 3u) {
        let number = numbers.clone();

        // spawnでスレッドを作り、スレッド内でprocを実行する
        spawn(proc() {
            let mut array = number.lock();

            (*array)[i] += 1;

            println!("numbers[{}] is {}", i, (*array)[i]);
        });
    }
}