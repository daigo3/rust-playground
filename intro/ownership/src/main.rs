fn main() {

    // vはvectorのオーナー
    let mut v = vec![];

    v.push("Hello");

    // vのオーナーをxに移しているので、以降変更ができなくなる
    //let x = &v[0];

    // 要素のコピーをxへ
    let x = v[0].clone();

    v.push("world");

    println!("{}", x);
}
